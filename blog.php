<?php include 'config/config.php'; ?>
<?php include 'constants/header.php'; ?>

<?php



    $response = null;
    $id = xss_clean($_GET['id']);
    $sql = "SELECT * FROM posts WHERE post_id = '$id'";
    $postData = mysqli_query($conn, $sql);
    $postDatasee = $postData->fetch_assoc();


    $user_id = xss_clean($postDatasee['user_id']);
    $sqlUser = "SELECT * FROM  users WHERE user_id = '$user_id'";
    $userData = mysqli_query($conn, $sqlUser);
    $userData = $userData->fetch_assoc();

    $categoryID = xss_clean($postDatasee['category_id']);
    $categorySql ="SELECT * FROM categories WHERE category_id = '$categoryID'";
    $categorysee = mysqli_query($conn, $categorySql);
    $categorysee = $categorysee->fetch_assoc();

    if(isset($userData['username'])){
        $postusername = $userData['username'];

    } else{
        $postusername = 'Removed User';
    }

if(isset($categorysee['category_name'])){
    $postcategoryname = $categorysee['category_name'];

} else{
    $postcategoryname = 'Removed Category';
}



    if (isset($_POST['commentSubmit'])){

        $comment = xss_clean($_POST['comment']);
        $commentUserID = xss_clean($_SESSION['user_id']);
        $insertSql = "INSERT INTO comments (comment, user_id, post_id)  VALUE('$comment', '$commentUserID', '$id')";
        mysqli_query($conn, $insertSql);

        $response = '<div class="alert alert-success">Your Commend Was Added</div>';

    }

    $sqlComment = "SELECT * FROM comments WHERE post_id = '$id' ";
    $commentData = mysqli_query($conn, $sqlComment);




?>

<div class="container">
    <div class="row">
        <h3 class="text-center mt-3"><?php echo $postDatasee['title']; ?></h3>
        
        <?php echo $response;?>
        <div class="col-sm-6 offset-3">
            <div class="card">
                <div class="card-img">
                    <img src="https://picsum.photos/536/354" class="card-img-top" alt="..." style="weight=100%">
                </div>

                <div class="postOwner">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="owner">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                                    <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                                </svg> </i>
                                <?php echo $postusername; ?>
                            </div>

                            <div class="date">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calendar-date" viewBox="0 0 16 16">
                                    <path d="M6.445 11.688V6.354h-.633A12.6 12.6 0 0 0 4.5 7.16v.695c.375-.257.969-.62 1.258-.777h.012v4.61h.675zm1.188-1.305c.047.64.594 1.406 1.703 1.406 1.258 0 2-1.066 2-2.871 0-1.934-.781-2.668-1.953-2.668-.926 0-1.797.672-1.797 1.809 0 1.16.824 1.77 1.676 1.77.746 0 1.23-.376 1.383-.79h.027c-.004 1.316-.461 2.164-1.305 2.164-.664 0-1.008-.45-1.05-.82h-.684zm2.953-2.317c0 .696-.559 1.18-1.184 1.18-.601 0-1.144-.383-1.144-1.2 0-.823.582-1.21 1.168-1.21.633 0 1.16.398 1.16 1.23z"/>
                                    <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z"/>
                                </svg>
                                <?php echo $postDatasee['post_date']; ?>
                            </div>

                            <div class="blogCategory">Category's:
                                <?php echo $postcategoryname;?>
                            </div>
                        </div>

                    </div>
                    <hr>
                </div>

                <div class="card-body">
                    <?php echo $postDatasee['post']; ?>
                </div>
            </div>
            <?php

                if(isset($_SESSION['login']) && ($_SESSION['user_id'] == $postDatasee['user_id'] || $_SESSION['auth'] == "admin" )){ ?>

                    <div class="operation">
                        <a class="btn btn-sm btn-danger mt-3" href="delete.php?id=<?php echo $id;?>">Delete Blog</a>
                        <a class="btn btn-sm btn-success mt-3" href="edit.php?id=<?php echo $id;?>">Edit Blog</a>
                    </div>

            <?php } ?>
            <?php
            if(isset($_SESSION["auth"]))  { ?>

                <form action = "" method = "post" class="mt-3 mb-5w" >
                <label > Comment:</label >
                <textarea class="form-control mb-3" name = "comment" ></textarea >
                <button class="btn btn-sm btn-info" name = "commentSubmit" > Submit</button >
            </form >
             <?php }?>

            <div>

                <ul class="list-group list-group-flush">


                    <?php

                        foreach ($commentData as $row){
                            $commentUserID = xss_clean($row['user_id']);
                            $comUserSql = "SELECT * FROM users WHERE user_id = '$commentUserID'";
                            $commentUserData = mysqli_query($conn, $comUserSql);

                            $commentUserData = $commentUserData->fetch_assoc();

                            if (isset($commentUserData)){
                                $commentuser = $commentUserData['username'];
                            }else{
                                $commentuser = "Removed User";
                            }

                            ?>
                            <li class="list-group-item"><?php echo $commentuser." : ". $row['comment']?>
                               <?php if(isset($_SESSION['login']) && ($_SESSION['user_id'] == $commentUserData['user_id'] || $_SESSION['auth'] == "admin" )){ ?>
                                    <a href="deletecomment.php?id=<?php echo $row['comment_id'];?>" class="btn btn-sm btn-danger">Delete</a>
                            <?php } ?>
                            </li>
                        <?php }
                     ?>




                </ul>
            </div>


        </div>




        

          

     
            
           

          


    </div>
</div>


<?php include 'constants/footer.php'; ?>



  