<?php include 'config/config.php'; ?>
<?php include 'constants/header.php'; ?>

<?php

    $sql = "SELECT * FROM posts ORDER BY post_id DESC LIMIT 10";
    $postData = mysqli_query($conn, $sql);
    $count = mysqli_num_rows($postData);

?>
<div>
    <?php

    if(isset($_SESSION["auth"])){
        if($_SESSION['auth'] == "admin"){ ?>

            <div class="container">
                <div class="col-sm-6 offset-3 text-center mt-3">
                    <a class="btn btn-lg btn-info" href="adminpanel.php">Admin Panel</a>
                </div>
            </div>

        <?php }} ?>
</div>

<div class="container">
    <div class="row">
        
        <h3 class="text-center mt-3">Blogs(<?php echo $count;?>)</h3>

        <?php 

            foreach($postData as $row){ ?>

        <div class="col-sm-4 mt-3">
        <div class="card" style="width: 18rem;">
            <img src="https://picsum.photos/536/354" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title"><?php echo $row['title']; ?></h5>
                <p class="card-text"><?php echo kisalt($row['post'], 100); ?></p>
                <a href="blog.php?id=<?php echo $row['post_id']; ?>" class="btn btn-primary">Read More...</a>
            </div>
        </div>
       </div>

            <?php }

        ?>

    </div>
</div>






<?php include 'constants/footer.php'; ?>



  