<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Blog</title>
  </head>
  <body>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="sabitler.php">Blog</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="index.php">Home</a>
        </li>
       
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Categories
          </a>


          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">

              <?php

                $categorySql = "SELECT * FROM categories";
                $categoryData = mysqli_query($conn, $categorySql);

                foreach ($categoryData as $row) {?>
                    <li><a class="dropdown-item" href="categorylist.php?category_id=<?php echo $row['category_id'];?>"><?php echo $row['category_name'];?></a></li>


             <?php } ?>

          </ul>
        </li>
          <?php if(isset($_SESSION["auth"])){
              if($_SESSION["auth"] == "admin" || $_SESSION["auth"] == "user") { ?>

          <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="create.php">Create Post</a>
          </li>

          <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="myposts.php">My Posts</a>
          </li>

           <?php }} ?>

      </ul>
      <form class="d-flex" action="search.php" method="post">
        <input class="form-control me-2" type="search" name="search" placeholder="Search Post" aria-label="Search">
        <button class="btn btn-outline-success" type="submit">Search</button>
      </form>

      <ul class="navbar-nav mb-2 mb-lg-0 mr-auto mr-3">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="#"> Welcome
            <?php
              if(isset( $_SESSION['username'])){
                echo $_SESSION['username'];
              }
            ?>
          </a>
        </li>
        <li class="nav-item">

          <?php
              if(isset( $_SESSION['login'])){
                echo '<a class="nav-link active" aria-current="page" href="logout.php">Logout</a>';
              }else{
                echo '<a class="nav-link active" aria-current="page" href="login.php">Login</a>';
              }
          ?>


        </li>
      </ul>



    </div>
  </div>
</nav>