<?php include 'config/config.php'; ?>
<?php include 'constants/header.php'; ?>

<?php
if(!isset($_SESSION['auth']) || $_SESSION['auth'] != "admin"){
    echo 'do not permission';
    exit;
}
?>

<?php

    $sql_user = "SELECT * FROM users";
    $userData = mysqli_query($conn, $sql_user);

    $sql_categories = "SELECT * FROM categories";
    $categoriesData = mysqli_query($conn, $sql_categories);




?>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 mt-3 text-center">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">User ID</th>
                        <th scope="col">Username</th>
                        <th scope="col">Operation</th>
                    </tr>
                    </thead>
                    <tbody>


                    <?php foreach ($userData as $row){ ?>
                    <th scope="row"><?php echo $row['user_id'];?></th>
                    <td><?php echo $row['username'];?></td>
                    <td>
                        <a href="deleteuser.php?id=<?php echo $row['user_id'];?>" class="btn btn-sm btn-danger">Delete</a>
                    </td>
                    </tr>

                    <?php } ?>


                    </tbody>
                </table>


                        <tr>

                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">Category ID</th>
                                    <th scope="col">Category</th>
                                    <th scope="col">Operation</th>
                                </tr>
                                </thead>
                                <tbody>


                                <?php foreach ($categoriesData as $row){ ?>
                                <th scope="row"><?php echo $row['category_id'];?></th>
                                <td><?php echo $row['category_name'];?></td>
                                <td>
                                    <a href="deletecategory.php?id=<?php echo $row['category_id'];?>" class="btn btn-sm btn-danger">Delete</a>
                                </td>
                                </tr>

                                <?php } ?>


                                </tbody>
                            </table>

                <tr>

                    <a class="btn btn-lg btn-primary" href="admincategories.php">Add Category</a>
            </div>
        </div>
    </div>


<?php include 'constants/footer.php'; ?>