<?php include 'config/config.php';

    if(isset($_POST['login'])){
        $username = xss_clean($_POST["username"]);
        $password = xss_clean($_POST["password"]);

        $logincheck = "SELECT * FROM users WHERE username = '$username' and password = '$password'";
        
        $control = mysqli_query($conn, $logincheck);
        $control_count = mysqli_num_rows($control);

        $userData = $control->fetch_assoc();
        
        if($control_count > 0){
            $_SESSION['username'] = $username;
            $_SESSION['login'] = true;
            $_SESSION['user_id'] = $userData['user_id'];
            
            if($username == "root"){
                $_SESSION['auth'] = "admin";
                header("Location:index.php");
                exit;
    
    
            }else{
                $_SESSION['auth'] = "user";
                header("Location:index.php");
                exit;
    
            }
        }
        else{
            echo 'password or username wrong';
        }
    
    }
    if(isset($_SESSION['login']) == true){
        header("Location:index.php");
        exit;
    }

        
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <!-- Bootstrap CSS -->
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Document</title>
</head>
<body>

    <div class="container">
        <div class="row">
            <h3 class="text-center mt-3">Login Page</h3>
            <div class="col-sm-6 offset-3">
                    
                <form action="" method= "post">
                    <div>
                        <label for="username">Username</label>
                        <input class="form-control mb-3" type="text" name="username">
                    </div>

                    <div>
                        <label for="password">Password</label>
                        <input class="form-control mb-3" type="password" name="password">
                    </div>

                    <div class="d-flex justify-content-between">
                        <button class="btn btn-sm btn-success" type="submit" name="login">Login</button>
                        <a class="btn btn-sm btn-info" href="index.php">Home</a>
                        <a class="btn btn-sm btn-primary" href="register.php">Register</a>
                    </div>
                 </form>
        
            </div>
        </div>
    </div>

    


</body>
</html>