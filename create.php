<?php include 'config/config.php'; ?>
<?php include 'constants/header.php'; ?>

<?php
    $response = null;
    $sqlCategory = "SELECT * FROM categories";
    $categoryData = mysqli_query($conn, $sqlCategory);

    if(isset($_POST["new_post"])) {
        $title = xss_clean($_POST['title']);
        $post = xss_clean($_POST['post']);

        $user_id = xss_clean($_SESSION['user_id']);
        $category_id = xss_clean($_POST['category']);
        $sql = "INSERT INTO posts (title, post, category_id, user_id) VALUES ('$title', '$post', '$category_id', '$user_id')";
        $insert = mysqli_query($conn, $sql);
        $response = "<div class='alert alert-success mt-5'> Post has been added</div>";
    }
?>




<form class="container col-sm-6 offset-3" method="POST">
    <?php echo $response; ?>

    <div>
        <label class="" for="Title">Title</label>
        <input class="form-control mb-3" type="text" name="title">
    </div>

    <div><label>Post</label><br>
    <textarea  class="form-control mb-3" name="post" type="text" > </textarea><br>
    </div>
    <label>Select Category:</label>
   <select name="category" class="form-control mb-3">

       <?php
            foreach ($categoryData as $row){ ?>
                <option value="<?php echo $row['category_id'];?>"><?php echo $row['category_name'];?></option>
            <?php }

       ?>
   </select>

    <div>
    <button class="btn btn-sm btn-primary" name="new_post" type="submit">submit</button>
    </div>
</form>


<?php include 'constants/footer.php'; ?>