<?php include 'config/config.php'; ?>
<?php include 'constants/header.php'; ?>

<?php

if(!isset($_SESSION['auth']) || $_SESSION['auth'] != "admin"){
    echo 'do not permission';
    exit;
}
?>

    <?php



        $response = null;

        if(isset($_POST["add_category"])) {
            $newCategory = xss_clean($_POST["categoryAdd"]);
            $sqlCategories = "INSERT INTO categories (category_name) VALUE ('$newCategory')";
            $dubCategory = "SELECT * FROM categories WHERE category_name = '$newCategory'";
            $checkCategory = mysqli_query($conn, $dubCategory);

            if (mysqli_num_rows($checkCategory) > 0) {
                $response = '<div class="alert alert-warning">that category exist</div>';

            } else {
                $insertCategory = mysqli_query($conn, $sqlCategories);
                echo $response = '<div class="alert alert-success">Category has been added</div>';
                header("Refresh:2; url=http://localhost:9000/project/adminpanel.php");
                exit();
            }
        }
    ?>

<?php
    if(isset($_POST["delete_category"])) {
        $categoryDelete = xss_clean($_POST["categorydelete"]);
        $sqlDeleteCategory = "DELETE FROM categories WHERE category_id = '$categoryDelete'";
        mysqli_query($conn, $sqlDeleteCategory);
        $response = '<div class="alert alert-warning">that category exist</div>';
        header("Refresh:2; url=admincategories.php");
        exit();
    }
?>


    <div class="container">
        <div class="row">
            <div class="col-sm-6 offset-3">
                <?php echo $response;?>

                <form action="" method= "post">
                    <div>
                        <label for="categoryadd">New Category</label>
                        <input class="form-control mb-3" type="text" name= "categoryAdd" id="categoryadd">
                    </div>

                    <div>
                        <button class="btn btn-sm btn-primary mb-5" type="submit" name="add_category">Add Category</button>
                    </div>
                    <hr>

                    <div>

                        <label class="mb-3" >Delete Category</label>
                        <select name="categorydelete" class="form-control mb-3" >

                            <?php
                            $sqlCategory = "SELECT * FROM categories";
                            $categoryData = mysqli_query($conn, $sqlCategory);

                            foreach ($categoryData as $row){ ?>
                                <option value="<?php echo $row['category_id'];?>"><?php echo $row['category_name'];?></option>
                            <?php }

                            ?>
                        </select>

                        <div>
                            <button class="btn btn-sm btn-danger mb-3" type="submit" name="delete_category">Delete Category</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>


<?php include 'constants/footer.php'; ?>