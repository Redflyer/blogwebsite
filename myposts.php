<?php

if(!isset($_SESSION['login'])){
}else {
    echo 'do not permission';
    exit();
}
?>
<?php include 'config/config.php'; ?>
<?php include 'constants/header.php'; ?>


    <?php
    $id = xss_clean($_SESSION["user_id"]);
    $sqlPosts="SELECT * FROM posts WHERE user_id = '$id'";
    $postsData = mysqli_query($conn, $sqlPosts);
    ?>

    <div class="container">
        <div class="row">
            <?php
            foreach ($postsData as $row){ ?>
                <div class="col-sm-4 mt-3">
                    <div class="card" style="width: 18rem;">
                        <img src="https://picsum.photos/536/354" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title"><?php echo $row['title']; ?></h5>
                            <p class="card-text"><?php echo kisalt($row['post'], 100); ?></p>
                            <a href="blog.php?id=<?php echo $row['post_id']; ?>" class="btn btn-primary">Read More...</a>
                        </div>
                    </div>
                </div>
            <?php }

            ?>

        </div>
    </div>


<?php include 'constants/footer.php'; ?>
