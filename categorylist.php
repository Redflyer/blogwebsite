<?php include 'config/config.php'; ?>
<?php include 'constants/header.php'; ?>

  <?php
        $category = xss_clean($_GET['category_id']);
        $sqlCategorylist = "SELECT * FROM posts WHERE category_id = '$category'";
        $oneCategoryData = mysqli_query($conn, $sqlCategorylist);

        $categorgoryDataSql = "SELECT * FROM categories WHERE category_id = '$category'";
        $categoryData = mysqli_query($conn, $categorgoryDataSql);
        $categoryData = $categoryData->fetch_assoc();

  ?>

<div class="container">
    <div class="row">

        <h3 class="text-center mt-3">Blogs <?php echo $categoryData['category_name'];?></h3>

        <?php

        foreach($oneCategoryData as $row){ ?>

            <div class="col-sm-4 mt-3">
                <div class="card" style="width: 18rem;">
                    <img src="https://picsum.photos/536/354" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title"><?php echo $row['title']; ?></h5>
                        <p class="card-text"><?php echo kisalt($row['post'], 100); ?></p>
                        <a href="blog.php?id=<?php echo $row['post_id']; ?>" class="btn btn-primary">Read More...</a>
                    </div>
                </div>
            </div>

        <?php }

        ?>

    </div>


<?php include 'constants/footer.php'; ?>
