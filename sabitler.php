<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>

<table class="table table-dark table-hover">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Value</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row">1</th>
        <td>Pi</td>
        <td><?php echo pi(); ?></td>

    </tr>

    <tr>
        <th scope="row">2</th>
        <td>Euler</td>
        <td><?php echo M_E; ?></td>

    </tr>

    <tr>
        <th scope="row">3</th>
        <td>Light Speed</td>
        <td><?php echo ' 299.792.458 m/s'; ?></td>

    </tr>

    <tr>
        <th scope="row">4</th>
        <td>Avagadro Number</td>
        <td><?php echo ' 6.02 x 10²³'; ?></td>

    </tr>

    </tbody>
</table>

</body>
</html>
