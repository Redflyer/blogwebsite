<?php include 'config/config.php'; ?>
<?php include 'constants/header.php'; ?>


<?php
    if(isset($_POST["search"])){
        $search = xss_clean($_POST["search"]);
        $sqlSearch = "SELECT * FROM posts WHERE post LIKE '%$search%'";
        $searchData = mysqli_query($conn, $sqlSearch);
    }
?>
<div class="container">
<div class="row">

    <h3 class="text-center mt-3">Blogs</h3>

    <?php

    foreach($searchData as $row){ ?>

        <div class="col-sm-4 mt-3">
            <div class="card" style="width: 18rem;">
                <img src="https://picsum.photos/536/354" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title"><?php echo $row['title']; ?></h5>
                    <p class="card-text"><?php echo kisalt($row['post'], 100); ?></p>
                    <a href="blog.php?id=<?php echo $row['post_id']; ?>" class="btn btn-primary">Read More...</a>
                </div>
            </div>
        </div>

    <?php }

    ?>

</div>
</div>


<?php include 'constants/footer.php'; ?>
